import { firebase } from '@react-native-firebase/firestore';


export const fireVouchers = firebase.firestore().collection('vouchers');

export const fireUsers = firebase.firestore().collection('accounts');