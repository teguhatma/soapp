import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { HomeScreen } from '../../../screen';
import ProductItem from '../../homeComponents/Products/TopUp/ProductItem';


const Stack = createStackNavigator();

const HomeNavigator = () => {
    return (
        <Stack.Navigator
            DetailinitialRouteName="HomeNavigator"
            screenOptions={{
                headerShown: false,
            }}
        >
            <Stack.Screen name="homeScreen" component={HomeScreen} />
            <Stack.Screen name="voucherScreen" component={ProductItem} />
        </Stack.Navigator>
    )
}

export default HomeNavigator
