import React, { useEffect, useState } from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { ChooseAuthScreen, LoginScreen, RegisterScreen } from '../../../screen/authentications';
import TabNavigator from '../BottomNavigation/TabNavigator';
import { firebase } from '@react-native-firebase/auth';


const Stack = createStackNavigator();

const AuthNavigator = () => {
    const [isSignedIn, setIsSignedIn] = useState(false)

    useEffect(() => {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                setIsSignedIn(true)
            }
            else (
                setIsSignedIn(false)
            )
        })
    })

    return (
        <Stack.Navigator
            DetailinitialRouteName="Authentication"
            screenOptions={{
                headerShown: false,
            }}
        >
            {isSignedIn === false ? (
                <>
                    <Stack.Screen name="chooseAuthScreen" component={ChooseAuthScreen} />
                    <Stack.Screen name="loginScreen" component={LoginScreen} />
                    <Stack.Screen name="registerScreen" component={RegisterScreen} />
                </>
            ) : (
                <Stack.Screen name="mainScreen" component={TabNavigator} />
            )}
        </Stack.Navigator>
    )
}

export default AuthNavigator
