import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { SettingScreen } from '../../../screen';
import AccountDetail from '../../settingComponents/AccountInformation/AccountDetail';
import PromoDetail from '../../settingComponents/PromoInformation/PromoDetail'
import { InfoAccount } from '../../settingComponents';

const Stack = createStackNavigator();

const SettingNavigator = () => {
    return (
        <Stack.Navigator DetailinitialRouteName="History">
            <Stack.Screen name="User Setting" component={SettingScreen} />
            <Stack.Screen name="Info Account" component={InfoAccount} />
            <Stack.Screen name="Account Detail" component={AccountDetail} />
            <Stack.Screen name="Promo Detail" component={PromoDetail} />
        </Stack.Navigator>
    )
}

export default SettingNavigator
