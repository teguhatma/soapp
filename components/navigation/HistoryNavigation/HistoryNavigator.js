import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import HistoryDetail from '../../historyComponents/Details/HistoryDetail';
import { HistoryScreen } from '../../../screen';

const Stack = createStackNavigator();

const HistoryNavigator = () => {
    return (
        <Stack.Navigator
            DetailinitialRouteName="History"
            screenOptions={{
                headerShown: false,
            }}
        >
            <Stack.Screen name="Transaction History" component={HistoryScreen} />
            <Stack.Screen name="History Detail" component={HistoryDetail} />
        </Stack.Navigator>
    )
}

export default HistoryNavigator
