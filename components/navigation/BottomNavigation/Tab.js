import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import Feather from 'react-native-vector-icons/Feather';



const Tab = ({ tab, onPress, icon }) => {
    return (
        <TouchableOpacity onPress={onPress} style={styles.tabPress}>
            {icon && <Feather name={icon} size={24} color="black" />}
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    tabPress: {
        padding: 20,
    }
})


export default Tab