import React, { useState } from 'react'
import { StyleSheet, View, Dimensions } from 'react-native'
import Tab from './Tab';

const { width } = Dimensions.get('screen')

const TabBar = ({ state, navigation }) => {
    const [selected, setSelected] = useState("home")
    const { routes } = state;

    const handlePress = activeTab => {
        setSelected(activeTab);
        navigation.navigate(activeTab)
    }
    return (
        <View style={styles.wrapper}>
            <View style={styles.container}>
                {routes.map((route, index) => (
                    <Tab
                        icon={route.params.icon}
                        tab={route}
                        onPress={() => handlePress(route.name)}
                        key={route.key}
                    />
                ))}
            </View>
        </View>
    )
}

export default TabBar

const styles = StyleSheet.create({
    wrapper: {
        position: 'absolute',
        bottom: 20,
        width,
    },
    container: {
        flexDirection: 'row',
        justifyContent: "space-between",
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: "#fff",
        borderRadius: 16
    }
})
