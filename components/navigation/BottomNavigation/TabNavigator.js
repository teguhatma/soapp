import React from 'react'
import { StyleSheet } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { ConfirmationScreen, HomeScreen } from '../../../screen'
import TabBar from './TabBar'
import HistoryNavigator from '../HistoryNavigation/HistoryNavigator'
import SettingNavigator from '../SettingNavigation/SettingNavigator'
import HomeNavigator from '../homeNavigation/HomeNavigator'

const Tab = createBottomTabNavigator()

const TabNavigator = () => {
    return (
        <Tab.Navigator tabBar={(props) => <TabBar {...props} />}>
            <Tab.Screen
                name="home"
                component={HomeNavigator}
                initialParams={{ icon: "home" }}
            />
            <Tab.Screen
                name="transaction"
                component={HistoryNavigator}
                initialParams={{ icon: "book-open" }}
            />
            <Tab.Screen
                name="confirmation"
                component={ConfirmationScreen}
                initialParams={{ icon: "clock" }}
            />
            <Tab.Screen
                name="user"
                component={SettingNavigator}
                initialParams={{ icon: "user" }}
            />
        </Tab.Navigator>
    )
}

export default TabNavigator

const styles = StyleSheet.create({})
