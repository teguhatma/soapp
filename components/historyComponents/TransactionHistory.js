import React from 'react'
import { View, Text, StyleSheet, Animated } from 'react-native'

const data = [
    "7 Hari",
    "14 Hari",
    "1 Bulan",
    "1 Tahun",
    "Semuanya"
]

const TransactionHistory = () => {
    return (
        <View>
            <Animated.FlatList
                data={data}
                keyExtractor={item => item.toString()}
                horizontal
                renderItem={({ item }) => {
                    return <View style={styles.selectedHistory}>
                        <Text style={[styles.poppins, styles.selectedHistoryText]}>{item}</Text>
                    </View>
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    poppinsBold: {
        fontFamily: 'poppinsBold'
    },
    poppins: {
        fontFamily: 'poppins'
    },
    selectedHistory: {
        marginTop: 10,
        marginBottom: 20,
        marginRight: 10,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 16,
        shadowColor: '#474747',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowRadius: 4,
        shadowOpacity: .1,
        // Shadow for IOS
        elevation: 1,
    },
    selectedHistoryText: {
        fontSize: 10,
        color: "#404040"
    }
})

export default TransactionHistory;