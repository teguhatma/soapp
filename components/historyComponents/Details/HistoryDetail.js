import React from 'react'
import { StyleSheet, Text, StatusBar, SafeAreaView, View } from 'react-native'
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import TopNavigation from './TopNavigation';
import { ScrollView } from 'react-native-gesture-handler';

const HistoryDetail = ({ navigation, route }) => {
    const { id, status, timestamp, type, via, total } = route.params.item;

    const RenderIcon = ({ type }) => {
        const size = 35;

        if (type === "Listrik") {
            return (
                <View style={[{ backgroundColor: "#095D6A" }, styles.bodyHistoryIcon]}>
                    <Entypo name="power-plug" size={size} color="#FBBC58" />
                </View>
            )
        } else if (type === "Pulsa") {
            return (
                <View style={[{ backgroundColor: "#FBBC58" }, styles.bodyHistoryIcon]}>
                    <Feather name="smartphone" size={size} color="#095D6A" />
                </View>
            )
        } else if (type === "Kuota") {
            return (
                <View style={[{ backgroundColor: "#F57B51" }, styles.bodyHistoryIcon]}>
                    <Entypo name="signal" size={size} color="#FDF6F0" />
                </View>
            )
        } else {
            return (
                <View style={[{ backgroundColor: "#FDF6F0" }, styles.bodyHistoryIcon]}>
                    <Feather name="credit-card" size={size} color="#F57B51" />
                </View>
            )
        }
    }
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: "#22314A", }}>
            <StatusBar barStyle="dark-content" backgroundColor="#22314A" />
            <ScrollView style={styles.wrapper} showsVerticalScrollIndicator={false}>
                <TopNavigation name={type} navigation={navigation} />
                <View style={styles.container}>
                    <RenderIcon type={type} />
                </View>
                <View style={{ marginBottom: 60 }}>
                    <View>
                        <View style={{ marginBottom: 5 }}>
                            <Text style={{ fontFamily: 'poppinsBold', fontSize: 15, color: "#EF7239" }}>Informasi Pemesanan</Text>
                        </View>
                        <View style={styles.container}>
                            <View style={styles.bodyInfo}>
                                <View style={styles.bookedInfo}>
                                    <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>Jenis:</Text>
                                    <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppins" }}>Listrik</Text>
                                </View>
                                <View style={styles.bookedInfo}>
                                    <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>No. Meter:</Text>
                                    <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppins" }}>081907823905</Text>
                                </View>
                                <View style={styles.bookedInfo}>
                                    <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>Nama:</Text>
                                    <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppins" }}>Lalu Anggara</Text>
                                </View>
                                <View style={styles.bookedInfo}>
                                    <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>Nominal:</Text>
                                    <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppins" }}>50.000</Text>
                                </View>
                                <View style={styles.bookedInfo}>
                                    <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>No. Token:</Text>
                                    <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppins" }}>225638141261080851</Text>
                                </View>
                                <View style={styles.bookedInfo}>
                                    <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>No. Pesanan:</Text>
                                    <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppins" }}>2021032617480333</Text>
                                </View>
                                <View style={styles.bookedInfo}>
                                    <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>Waktu Pemesanan:</Text>
                                    <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppins" }}>{timestamp}</Text>
                                </View>
                            </View>
                        </View>
                    </View>

                    <View>
                        <View style={{ marginBottom: 5 }}>
                            <Text style={{ fontFamily: 'poppinsBold', fontSize: 15, color: "#EF7239" }}>Informasi Pembayaran</Text>
                        </View>
                        <View style={styles.container}>
                            <View style={styles.bodyInfo}>
                                <View style={styles.bookedInfo}>
                                    <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>Nominal:</Text>
                                    <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppins" }}>Rp. 50.000</Text>
                                </View>
                                <View style={styles.bookedInfo}>
                                    <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>Biaya Admin:</Text>
                                    <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppins" }}>Rp. 2.500</Text>
                                </View>
                                <View style={styles.bookedInfo}>
                                    <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>Jumlah:</Text>
                                    <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppins" }}>Rp. 52.500</Text>
                                </View>
                            </View>
                        </View>
                    </View>

                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

export default HistoryDetail

const styles = StyleSheet.create({
    wrapper: {
        paddingLeft: 20,
        marginTop: 20,
        paddingRight: 20,
        marginBottom: 40,
    },
    container: {
        backgroundColor: "#fff",
        borderRadius: 16,
        marginBottom: 10,
    },
    poppinsBold: {
        fontFamily: 'poppinsBold'
    },
    poppins: {
        fontFamily: 'poppins'
    },
    statusSuccess: {
        backgroundColor: 'green',
        borderRadius: 100,
        height: 25,
        width: 25,
        borderWidth: 4,
        borderColor: "#fff",
        marginLeft: -15
    },
    statusFailure: {
        backgroundColor: 'red',
        borderRadius: 100,
        height: 25,
        width: 25,
        borderColor: "#fff",
        borderWidth: 4,
        marginLeft: -15
    },
    bodyHistoryIcon: {
        alignItems: 'center',
        padding: 40,
        borderRadius: 14,
        margin: 15,
    },
    bookedInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 8,
    },
    bodyInfo: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20,
        paddingBottom: 12,
    }
})
