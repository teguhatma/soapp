import React from 'react'
import { StyleSheet, Text } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';

const TopNavigation = ({ navigation, name }) => {
    return (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginBottom: 10, flexDirection: "row" }}>
            <Ionicons name="chevron-back-circle-outline" size={25} color="#EF7239" />
            <Text style={styles.textNav}>{name}</Text>
        </TouchableOpacity>
    )
}

export default TopNavigation

const styles = StyleSheet.create({
    textNav: {
        fontFamily: "poppinsBold",
        fontSize: 18,
        color: "#EF7239",
        marginLeft: 5,
    }
})
