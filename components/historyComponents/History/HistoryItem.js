import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import { data } from './data'

const HistoryItem = ({ navigation }) => {
    const RenderStatus = ({ status }) => {
        if (status === "Sukses") {
            return (
                <View style={styles.statusSuccess}>
                </View>
            )
        } else {
            return (
                <View style={styles.statusFailure}>
                </View>
            )
        }
    }

    const RenderIcon = ({ type }) => {
        if (type === "Listrik") {
            return (
                <View style={[{ backgroundColor: "#095D6A" }, styles.bodyHistoryIcon]}>
                    <Entypo name="power-plug" size={25} color="#FBBC58" />
                </View>
            )
        } else if (type === "Pulsa") {
            return (
                <View style={[{ backgroundColor: "#FBBC58" }, styles.bodyHistoryIcon]}>
                    <Feather name="smartphone" size={25} color="#095D6A" />
                </View>
            )
        } else if (type === "Kuota") {
            return (
                <View style={[{ backgroundColor: "#F57B51" }, styles.bodyHistoryIcon]}>
                    <Entypo name="signal" size={25} color="#FDF6F0" />
                </View>
            )
        } else {
            return (
                <View style={[{ backgroundColor: "#FDF6F0" }, styles.bodyHistoryIcon]}>
                    <Feather name="credit-card" size={25} color="#F57B51" />
                </View>
            )
        }
    }

    const handlePress = item => {
        if (item.status === "Sukses") {
            navigation.navigate("History Detail", {
                item
            })
        } else {
            alert("Tidak ada Informasi karena Gagal")
        }
    }

    return (
        <View>
            {data.map(item => (
                <TouchableOpacity style={styles.wrapper} onPress={() => handlePress(item)} key={item.id}>
                    <View style={styles.container}>
                        <View style={styles.bodyHistory}>
                            <RenderIcon type={item.type} />
                            <View style={styles.historyInfo}>
                                <View>
                                    <Text style={[styles.poppinsBold, styles.typeText]}>{item.type}</Text>
                                    <Text style={[styles.poppins, styles.viaText]}>{item.via}</Text>
                                </View>
                                <View>
                                    <Text style={[styles.poppins, styles.totalText]}>{item.total}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.headerHistory}>
                        <RenderStatus status={item.status} />
                    </View>
                </TouchableOpacity>
            ))}
        </View>
    )
}

export default HistoryItem

const styles = StyleSheet.create({
    headerHistory: {
    },
    wrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 20,
        borderRadius: 16,
    },
    container: {
        backgroundColor: '#fff',
        flex: 1,
        borderRadius: 20,
        paddingLeft: 10,
        paddingBottom: 10,
        paddingTop: 10,
        paddingRight: 30,
    },
    bodyHistory: {
        flexDirection: 'row',
    },
    historyInfo: {
        flexDirection: "row",
        alignItems: 'center',
        flex: 1,
        justifyContent: 'space-between'
    },
    bodyHistoryIcon: {
        alignItems: 'center',
        padding: 15,
        borderRadius: 16,
        marginRight: 15
    },
    poppinsBold: {
        fontFamily: 'poppinsBold'
    },
    poppins: {
        fontFamily: 'poppins'
    },
    statusText: {
        color: '#fff',
        fontSize: 10
    },
    statusSuccess: {
        backgroundColor: 'green',
        borderRadius: 100,
        height: 25,
        width: 25,
        borderWidth: 4,
        borderColor: "#fff",
        marginLeft: -15
    },
    statusFailure: {
        backgroundColor: 'red',
        borderRadius: 100,
        height: 25,
        width: 25,
        borderColor: "#fff",
        borderWidth: 4,
        marginLeft: -15
    },
    timestamp: {
        fontSize: 12,
        color: "#404040"
    },
    typeText: {
        fontSize: 18,
        color: '#404040'
    },
    viaText: {
        fontSize: 12,
        color: "#8E8E8E"
    },
    totalText: {
        fontSize: 14,
        color: "#404040"
    }
})
