import React from 'react'
import { View } from 'react-native'
import HistoryItem from './HistoryItem';


const HistoryCard = ({ navigation }) => {
    return (
        <View style={{ marginBottom: 30 }}>
            <HistoryItem
                navigation={navigation}
            />
        </View>
    )
}

export default HistoryCard
