import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const TopNavigation = ({ navigation }) => {
    return (
        <View style={{ marginBottom: 10 }}>
            <Text style={{ fontFamily: 'poppinsBold', fontSize: 18, color: "#EF7239" }}>Transaction History</Text>
        </View>
    )
}

export default TopNavigation

const styles = StyleSheet.create({})
