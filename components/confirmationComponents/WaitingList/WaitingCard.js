import React from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'
import { data } from './data'
import { Entypo } from 'react-native-vector-icons'


export const WaitingCard = () => {
    if (data == 0) {
        return (
            <View style={{
                justifyContent: 'center', alignItems: 'center', paddingTop: 40,
            }}>
                <Entypo name="emoji-sad" size={120} color="#404040" />
                <Text style={{ fontSize: 14, color: "#404040", fontFamily: "poppinsBold" }}>Waduh, belum ada transaksi nih</Text>
                <Text style={{ fontSize: 12, color: "#404040", fontFamily: "poppins" }}>Siapa tau ada yang cocok</Text>
                <View style={{
                    borderRadius: 16, backgroundColor: "#58CBA2", shadowColor: '#474747',
                    shadowOffset: {
                        width: 0,
                        height: 4,
                    },
                    marginBottom: 5,
                    marginTop: 5,
                    shadowRadius: 4,
                    shadowOpacity: .2,
                    // Shadow for IOS
                    elevation: 2,
                }}>
                    <View style={{ padding: 10, alignItems: 'center', justifyContent: "center" }} ><Text style={{ color: "#fff", fontFamily: "poppins" }}>Lihat Paket</Text></View>
                </View>
            </View >
        )
    } else {
        return (
            <View style={{ marginBottom: 50 }}>
                <View style={{ marginBottom: 10 }}>
                    <Text style={{ fontFamily: 'poppinsBold', fontSize: 18, color: "#EF7239" }}>Waiting Payment</Text>
                </View>
                {data.map(data => (
                    <View style={styles.container} key={data.id}>
                        <View style={styles.viewTopUp}>
                            <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppinsBold" }}>23 Jam 59 Menit 48 Detik</Text>
                            <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppinsBold" }}>Rp. 50.000</Text>
                        </View>
                        <View style={{ flexDirection: "row", paddingLeft: 20, paddingRight: 20 }}>
                            <View style={{ paddingRight: 15, paddingTop: 20, paddingBottom: 20, alignItems: 'center' }}>
                                <Image source={{
                                    uri: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fid%2Fthumb%2F5%2F55%2FBNI_logo.svg%2F1280px-BNI_logo.svg.png&f=1&nofb=1"
                                }} style={{
                                    width: 60, height: 25, resizeMode: "contain"
                                }} />
                            </View>
                            <View style={{ flex: 1 }}>
                                <View style={{ flexDirection: "row", justifyContent: "space-between", marginBottom: 10 }}>
                                    <View>
                                        <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>Pembayaran:</Text>
                                        <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppinsBold" }}>Listrik</Text>
                                    </View>
                                    <View>
                                        <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>Penerima:</Text>
                                        <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppinsBold" }}>081907823905</Text>
                                    </View>
                                </View>
                                <View>
                                    <Text style={{ color: "#404040", fontSize: 12, fontFamily: "poppins" }}>No. Virtual Account:</Text>
                                    <Text style={{ color: "#404040", fontSize: 13, fontFamily: "poppinsBold" }}>8804-4600-2753-170</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ padding: 20 }}>
                            <View style={{ borderRadius: 16, backgroundColor: "#fff", borderWidth: 1, borderColor: "#DCDBDB", marginBottom: 10 }}>
                                <View style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 9, paddingBottom: 9, alignItems: 'center', justifyContent: "center" }} ><Text style={{ color: "#404040", fontFamily: "poppins" }}>Metode Pembayaran</Text></View>
                            </View>
                            <View style={{
                                borderRadius: 16, backgroundColor: "#58CBA2", shadowColor: '#474747',
                                shadowOffset: {
                                    width: 0,
                                    height: 4,
                                },
                                marginBottom: 5,
                                shadowRadius: 4,
                                shadowOpacity: .2,
                                // Shadow for IOS
                                elevation: 2,
                            }}>
                                <View style={{ padding: 10, alignItems: 'center', justifyContent: "center" }} ><Text style={{ color: "#fff", fontFamily: "poppins" }}>Lanjut Pembayaran</Text></View>
                            </View>
                        </View>
                    </View>
                ))
                }
            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        borderRadius: 16,
        marginBottom: 20
    },
    viewTopUp: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 20,
        padding: 20,
        borderBottomWidth: 1,
        borderColor: "#DCDBDB"
    }
})