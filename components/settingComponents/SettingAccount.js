import React from 'react'
import { Text, View, StyleSheet } from 'react-native'


export const SettingAccount = () => {
    return (
        <View>
            <Text style={style.pengaturanAkun}>Info Account</Text>
        </View>

    )
}

const style = StyleSheet.create({
    pengaturanAkun: {
        fontSize: 16,
        marginTop: 20,
        marginBottom: 10,
        fontFamily: 'poppinsBold',
        color: "#EF7239"
    }
})