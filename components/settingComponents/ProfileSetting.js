import * as React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

export const ProfileSetting = () => {
    return (
        <View style={styles.shadow}>
            <View style={{ borderRadius: 16, marginBottom: 15 }}>
                <Image source={
                    require("../../assets/img/darul.png")
                }
                    style={{
                        width: 150,
                        height: 150,
                        resizeMode: 'cover',
                        borderRadius: 16
                    }}
                />
            </View>

            <Text style={styles.userName}>ROBBY DARMAWAN PRATAMA SAPUTRA JAYA</Text>
            <Text style={{ fontSize: 14, fontFamily: 'poppins', color: "#404040" }}>+62 8515 7999 43</Text>
        </View>
    )

}


const styles = StyleSheet.create({
    userName: {
        fontSize: 18,
        fontFamily: 'poppinsBold',
        color: '#404040',
        textAlign: 'center'

    },
    shadow: {
        borderRadius: 16,
        backgroundColor: '#fff',
        alignItems: "center",
        justifyContent: 'center',
        padding: 25,
    }

});

