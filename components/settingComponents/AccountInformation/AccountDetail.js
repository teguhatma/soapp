import React from 'react'
import { View, Text, SafeAreaView, ScrollView, StyleSheet } from 'react-native'



const AccountDetail = () => {
    return (
        <SafeAreaView style={styles.wrapper}>
            <ScrollView style={styles.container}>
                <Text style={styles.label}>Pengaturan Akun</Text>
                <View style={styles.buttonView}>
                    <Text style={styles.textLabel}>darulaqli@gmail.com</Text>
                </View>
                <View style={styles.buttonView}>
                    <Text style={styles.textLabel}>Darl Akli Junaidi</Text>
                </View>
                <View style={styles.buttonView}>
                    <Text style={styles.textLabel}>+6285-9635-3966</Text>
                </View>
                <View>
                    <Text>Edit</Text>
                </View>
                <View>
                    <Text>Simpan</Text>
                </View>
            </ScrollView>
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: "#22314A",
    },
    container: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 60,
    },
    buttonView: {
        backgroundColor: '#fff',
        marginBottom: 20,
        borderRadius: 10,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20,
        paddingBottom: 20,
    },
    label: {
        fontSize: 18,
        color: '#ef7239',
        marginBottom: 20,
        fontFamily: 'poppinsBold',

    },
    textLabel: {
        fontFamily: 'poppins',
        fontSize: 14,
    }
})
export default AccountDetail
