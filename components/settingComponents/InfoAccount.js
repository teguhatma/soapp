import React from 'react'
import { Text, StyleSheet, TouchableOpacity } from 'react-native'
import { AntDesign } from 'react-native-vector-icons';

export const InfoAccount = ({ data, navigation }) => {
    return (
        <TouchableOpacity style={style.viewInfo} onPress={() => navigation.navigate("Account Information")}>
            <Text style={{ fontFamily: 'poppins', color: "#404040" }}>{data}</Text>
            <AntDesign name="right" size={14} color="black" />
        </TouchableOpacity>
    )
}

const style = StyleSheet.create({
    viewInfo: {
        backgroundColor: '#fff',
        marginBottom: 20,
        borderRadius: 10,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20,
        paddingBottom: 20,
    }

})