import { firebase } from '@react-native-firebase/auth';
import React from 'react'
import { TouchableOpacity, Text, StyleSheet, View } from 'react-native'
import AntDesign from 'react-native-vector-icons/AntDesign';

const InfoAkun = ({ navigation }) => {
    const SignOut = () => {
        firebase.auth().signOut().then(() => {
            navigation.navigate("chooseAuthScreen")
        }).catch((error) => {
            alert(error)
        })
    }

    return (
        <View>
            <View>
                <TouchableOpacity style={styles.infoView} onPress={() => navigation.navigate("Account Detail")}>
                    <Text>Info Akun</Text>
                    <AntDesign name="right" size={14} color="black" />
                </TouchableOpacity>
            </View>
            <View>
                <TouchableOpacity style={styles.infoView} onPress={() => navigation.navigate("Promo Detail")}>
                    <Text>Lihat Promo</Text>
                    <AntDesign name="right" size={14} color="black" />
                </TouchableOpacity>
            </View>
            <View>
                <TouchableOpacity style={styles.infoView} onPress={() => navigation.navigate("Promo Detail")}>
                    <Text>Undang Teman</Text>
                    <AntDesign name="right" size={14} color="black" />
                </TouchableOpacity>
                <TouchableOpacity style={styles.infoView} onPress={() => SignOut()}>
                    <Text>Keluar</Text>
                </TouchableOpacity>
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    infoView: {
        backgroundColor: '#fff',
        marginBottom: 20,
        borderRadius: 10,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20,
        paddingBottom: 20,
    }
})

export default InfoAkun