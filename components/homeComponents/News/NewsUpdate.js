import React from 'react'
import { Text, View, Image } from 'react-native'
import { data } from './data'

export const NewsUpdate = () => {
    return (
        <View style={{ marginBottom: 50 }}>
            <View style={{ marginBottom: 10 }}>
                <Text style={{ fontFamily: 'poppinsBold', fontSize: 18, color: "#EF7239" }}>Important thing's</Text>
            </View>
            {data.map((item) => {
                return (
                    <View key={item.id} style={{
                        flex: 1,
                        marginBottom: 20,
                        backgroundColor: '#fff', borderRadius: 16,
                    }}>
                        <Image
                            source={{
                                uri: item.imgUrl,
                            }}
                            style={{
                                height: 120,
                                resizeMode: 'cover',
                                borderTopLeftRadius: 16,
                                borderTopRightRadius: 16,
                            }}
                        />
                        <View style={{ padding: 15 }}>
                            <Text style={{ fontSize: 12, fontFamily: 'poppinsBold', color: "#404040" }}>{item.title}</Text>
                            <Text style={{ fontSize: 9, fontFamily: 'poppins', color: "#8e8e8e" }}>{item.timestamp}</Text>
                        </View>

                    </View>
                )
            })}
        </View>
    )
}

