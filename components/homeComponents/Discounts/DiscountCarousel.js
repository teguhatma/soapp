import React from 'react'
import { View, Image, Dimensions, Animated, Text, StyleSheet } from 'react-native'
import { data } from './data'

const { width } = Dimensions.get('screen');
const imageW = width * 0.7;
const imageH = imageW * 0.5;

const DiscountCarousel = () => {
    return (
        <View>
            <View style={{ marginBottom: 10 }}>
                <Text style={{ fontFamily: 'poppinsBold', fontSize: 18, color: "#EF7239" }}>Grab the discount's</Text>
            </View>
            <Animated.FlatList
                data={data}
                keyExtractor={item => item.toString()}
                horizontal
                pagingEnabled
                renderItem={({ item }) => {
                    return (
                        <View style={styles.container}>
                            <View>
                                <Text style={{ fontFamily: 'poppinsBold', fontSize: 14, color: '#fff' }}>Top up 20%</Text>
                            </View>
                            <Image
                                source={require('../../../assets/discountImg/mobile.png')}
                                style={{
                                    width: 100,
                                    height: 100,
                                    resizeMode: 'contain',
                                    borderRadius: 16,
                                }}
                            />
                        </View>
                    )
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#6A63DD",
        marginRight: 20,
        width: imageW,
        flexDirection: "row",
        justifyContent: "space-between",
        borderRadius: 20,
        padding: 15,
        marginBottom: 10,
        alignItems: 'center'
    }
})

export default DiscountCarousel