import React, { useState, useEffect } from 'react'
import { StyleSheet, ScrollView, StatusBar, SafeAreaView, View, Text } from 'react-native'
import { fireVouchers } from '../../../../services/firebase'
import { firebase } from '@react-native-firebase/firestore';
import database from '@react-native-firebase/database';

const ProductItem = ({ route }) => {
    const { name } = route.params
    const nameLow = name.toLowerCase();
    const [vouchers, setVouchers] = useState([])

    database().ref('/voucherDistrict/rayana.net').once('value').then(snapshot => {
        console.log("data: ", snapshot.val())
    })

    return (
        <SafeAreaView style={styles.wrapper}>
            <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
                <StatusBar barStyle="dark-content" backgroundColor="#22314A" />
                <View>
                    <Text>{name} Screen</Text>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

export default ProductItem

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: "#22314A",
    },
    container: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 40,
    }
})