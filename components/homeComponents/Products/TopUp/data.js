export const data = [{
    id: 1,
    icon: "smartphone",
    iconType: 'Feather',
    name: "Pulsa",
    background: "#FBBC58",
    color: "#095D6A"
},
{
    id: 2,
    icon: "power-plug",
    iconType: 'Entypo',
    name: "Listrik",
    background: "#095D6A",
    color: "#FBBC58"
},
]

export const data2 = [{
    id: 3,
    icon: "signal",
    iconType: 'Entypo',
    name: "Kuota",
    background: "#F57B51",
    color: "#FDF6F0"
},
{
    id: 4,
    icon: "credit-card",
    iconType: 'Feather',
    name: "Rayana.net",
    background: "#FDF6F0",
    color: "#F57B51"
}
]