import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Dimensions } from 'react-native'
import { data } from './data'
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { fireVouchers } from '../../../../services/firebase'


const { width } = Dimensions.get('screen')

const RenderItemIcon = ({ iconType, icon, color }) => {
    if (iconType === "Feather") {
        return (
            <View style={styles.cardProduct}>
                <Feather name={icon} size={25} color={color} />
            </View>
        )
    } else {
        return (
            <View style={styles.cardProduct}>
                <Entypo name={icon} size={25} color={color} />
            </View>
        )
    }
}


export default function TopUpItem({ navigation }) {
    const [vouchers, setVouchers] = useState([])

    function getVoucher() {
        fireVouchers.onSnapshot((querySnapshot) => {
            const items = [];
            querySnapshot.forEach((doc) => {
                items.push(doc.data());
            });
            setVouchers(items)
        })
    }

    useEffect(() => {
        getVoucher();
    }, [])

    const handlePress = (voucher) => {
        navigation.navigate("voucherScreen", voucher)
    }

    return (
        <View style={{ marginTop: 10 }}>
            <View style={{ marginBottom: 10 }}>
                <Text style={{ fontFamily: 'poppinsBold', fontSize: 18, color: "#EF7239" }}>Top up</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20 }}>
                {data.map(data => {
                    return (
                        <View key={data.id} style={[styles.card, { backgroundColor: data.background }]}>
                            <RenderItemIcon iconType={data.iconType} icon={data.icon} color={data.color} />
                            <Text style={{ fontSize: 12, fontFamily: 'poppins', color: data.color }}>{data.name}</Text>
                        </View>
                    )
                })}
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20 }}>
                {vouchers.map(voucher => {
                    return (
                        <TouchableOpacity
                            key={voucher.name}
                            style={
                                [styles.card, { backgroundColor: data.background }]
                            }
                            onPress={() => handlePress(voucher)}
                        >
                            {/* <RenderItemIcon iconType={data.iconType} icon={data.icon} color={data.color} /> */}
                            <Text style={{ fontSize: 12, fontFamily: 'poppins', color: data.color }}>{voucher.name}</Text>
                        </TouchableOpacity>
                    )
                })}
            </View>
        </View >
    )
}


const styles = StyleSheet.create({
    card: {
        alignItems: 'center',
        borderRadius: 16,
        padding: 30,
        width: width * 0.8 / 2,
    },
    cardProduct: {
        marginBottom: 5
    }
})