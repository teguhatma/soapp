import React from 'react'
import { View } from 'react-native'
import DiscountCarousel from '../Discounts/DiscountCarousel'
import TopUpItem from './TopUp/TopUpItem'

export const Products = ({ navigation }) => {
    return (
        <View>
            <DiscountCarousel />
            <TopUpItem navigation={navigation} />
        </View>
    )
}

