import * as React from 'react';
import { useFonts } from 'expo-font';
import { Text } from 'react-native'

export default function PoppinsBold(props) {
    const [loaded] = useFonts({
        poppinsBold: require("../../assets/fonts/Poppins-Bold.ttf"),
    });

    if (!loaded) {
        return null;
    }

    return (
        <Text style={{ fontFamily: 'poppinsBold' }} {...props}>{props.children}</Text>
    );
}