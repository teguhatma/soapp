import * as React from 'react';
import { useFonts } from 'expo-font';
import { Text } from 'react-native'

export default function Poppins(props) {
    const [loaded] = useFonts({
        poppins: require("../../assets/fonts/Poppins-Regular.ttf")
    });

    if (!loaded) {
        return null;
    }

    return (
        <Text style={{ fontFamily: 'poppins' }} {...props}>{props.children}</Text>
    );
}