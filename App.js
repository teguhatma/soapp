/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
// import TabNavigator from './components/navigation/BottomNavigation/TabNavigator';
import AuthNavigator from './components/navigation/authNavigation/AuthNavigator';


export default function App() {
  return (
    <NavigationContainer >
      <AuthNavigator />
    </NavigationContainer >
  );
};