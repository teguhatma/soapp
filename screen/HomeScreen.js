import React from 'react'
import { StyleSheet, ScrollView, StatusBar, SafeAreaView } from 'react-native'
import { Products, NewsUpdate } from '../components/homeComponents'

export const HomeScreen = ({ navigation }) => {
    return (
        <SafeAreaView style={styles.wrapper}>
            <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
                <StatusBar barStyle="dark-content" backgroundColor="#22314A" />
                <Products navigation={navigation} />
                <NewsUpdate />
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: "#22314A",
    },
    container: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 40,
    }
})