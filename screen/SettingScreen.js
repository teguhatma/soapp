import React from 'react'
import { StyleSheet, ScrollView, SafeAreaView, StatusBar } from 'react-native'
import { SettingAccount, ProfileSetting,} from '../components/settingComponents'
import InfoAkun from '../components/settingComponents/InfoAkun/InfoAkun'



export const SettingScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.wrapper}>
            <ScrollView style={styles.container}>
                <StatusBar barStyle="dark-content" backgroundColor="#22314A" />
                <ProfileSetting />
                <SettingAccount />
                {/* <InfoAccount navigation={navigation}/> */}
                <InfoAkun navigation={navigation}/>
            </ScrollView>
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: "#22314A",
    },
    container: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 60,
    }
})