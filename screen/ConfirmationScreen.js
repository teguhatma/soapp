import React from 'react'
import { StyleSheet, ScrollView, SafeAreaView, StatusBar } from 'react-native'
import { WaitingCard } from '../components/confirmationComponents'


export const ConfirmationScreen = () => {
    return (
        <SafeAreaView style={styles.wrapper}>
            <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
                <StatusBar barStyle="dark-content" backgroundColor="#22314A" />
                <WaitingCard />
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: "#22314A",
    },
    container: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 40,
    }
})