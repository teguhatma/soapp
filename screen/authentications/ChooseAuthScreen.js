import React from 'react'
import { Text, View, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native'

export const ChooseAuthScreen = ({ navigation }) => {
    return (
        <SafeAreaView style={styles.wrapper}>
            <View style={styles.main}>
                <View style={styles.content}>
                    <View>
                        <TouchableOpacity style={{
                            borderRadius: 16,
                            backgroundColor: "#22314A",
                            borderWidth: 1,
                            borderColor: "#22314A",
                            marginBottom: 40,
                            marginRight: 15,
                            marginTop: 40,
                            marginLeft: 15
                        }}
                            onPress={() => navigation.navigate("loginScreen")}
                        >
                            <View style={{
                                paddingTop: 12, paddingBottom: 12, alignItems: 'center', justifyContent: "center"
                            }} >
                                <Text style={{ color: "#fff" }}>Masuk</Text>
                            </View>
                        </TouchableOpacity>
                        <View>
                            <Text style={{ textAlign: "center", fontWeight: "bold" }}>
                                Atau
                            </Text>
                        </View>
                        <TouchableOpacity style={{
                            borderRadius: 16,
                            borderWidth: 1,
                            borderColor: "#22314A",
                            marginBottom: 40,
                            marginRight: 15,
                            marginTop: 40,
                            marginLeft: 15
                        }}
                            onPress={() => navigation.navigate("registerScreen")}
                        >
                            <View style={{
                                paddingTop: 12, paddingBottom: 12, alignItems: 'center', justifyContent: "center"
                            }} >
                                <Text style={{ color: "#22314A" }}>Mendaftar</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        margin: 20,
    },
    container: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 40,
    },
    main: {
        flex: 1,
        justifyContent: "center",
        alignItems: "stretch"
    },
    content: {
    },
    input: {
        paddingTop: 9,
        paddingBottom: 9,
        paddingLeft: 15,
        paddingRight: 15,
        color: "#22314A",
        borderRadius: 16,
        borderWidth: 1,
        borderColor: "#22314A",
        fontSize: 14,
    },
})