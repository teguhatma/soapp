import React from 'react'
import { Text, StyleSheet, ScrollView, StatusBar, SafeAreaView } from 'react-native'

export const RegisterScreen = () => {
    return (
        <SafeAreaView style={styles.wrapper}>
            <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
                <StatusBar barStyle="dark-content" backgroundColor="#22314A" />
                <Text>Register Screen</Text>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: "#22314A",
    },
    container: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 40,
    }
})