import React, { useState, useEffect } from 'react';
import { Button, TextInput } from 'react-native';
import auth from '@react-native-firebase/auth';
import { Text, View, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native'

export function LoginScreen({ navigation }) {
    // If null, no SMS has been sent
    const [confirm, setConfirm] = useState(null);
    const [number, setNumber] = useState('')
    const [code, setCode] = useState('');

    // Handle the button press
    async function signInWithPhoneNumber(phoneNumber) {
        const confirmation = await auth().signInWithPhoneNumber("+62" + phoneNumber);
        setConfirm(confirmation);
    }

    async function confirmCode() {
        try {
            await confirm.confirm(code);
            navigation.navigate("mainScreen")
        } catch (error) {
            alert("Invalid OTP.")
        }
    }

    if (!confirm) {
        return (
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.main}>
                    <View style={styles.content}>
                        <View>
                            <TextInput
                                value={number}
                                onChangeText={text => setNumber(text)}
                                keyboardType="numeric"
                            />
                            <TouchableOpacity style={{
                                borderRadius: 16,
                                backgroundColor: "#22314A",
                                borderWidth: 1,
                                borderColor: "#22314A",
                                marginBottom: 40,
                                marginRight: 15,
                                marginTop: 40,
                                marginLeft: 15
                            }}
                                onPress={() => signInWithPhoneNumber(number)}
                            >
                                <View style={{
                                    paddingTop: 12, paddingBottom: 12, alignItems: 'center', justifyContent: "center"
                                }} >
                                    <Text style={{ color: "#fff" }}>Masuk</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        );
    }

    return (
        <>
            <TextInput value={code} onChangeText={text => setCode(text)} keyboardType="number-pad" />
            <Button title="Confirm Code" onPress={() => confirmCode()} />
        </>
    );
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        margin: 20,
    },
    container: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 40,
    },
    main: {
        flex: 1,
        justifyContent: "center",
        alignItems: "stretch"
    },
    content: {
    },
    input: {
        paddingTop: 9,
        paddingBottom: 9,
        paddingLeft: 15,
        paddingRight: 15,
        color: "#22314A",
        borderRadius: 16,
        borderWidth: 1,
        borderColor: "#22314A",
        fontSize: 14,
    },
})