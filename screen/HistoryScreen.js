import React from 'react'
import { SafeAreaView, ScrollView, StatusBar, StyleSheet, View } from 'react-native'
import HistoryCard from '../components/historyComponents/History/HistoryCard'
import TopNavigation from '../components/historyComponents/TopNavigation'

export const HistoryScreen = ({ navigation }) => {
    return (
        <SafeAreaView style={styles.wrapper}>
            <StatusBar barStyle="dark-content" backgroundColor="#22314A" />
            <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
                <TopNavigation navigation={navigation} />
                <HistoryCard navigation={navigation} />
            </ScrollView>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: "#22314A"
    },
    container: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 60,
    }
})